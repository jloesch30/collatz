#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def get_cycle(num1, num2, max_cycle=0, add_to_cache=False, c=None):
    assert num1 <= num2, "num1 must be larger or equal to num2"
    assert (type(c) == dict or c == None), "c not valid"
    assert (type(num1) == int and type(num2) == int), "input (num1 or num2) not valid"
    if add_to_cache:
        curr_interval_max = 0
    for k in range(num1, num2+1):
        curr_cycle = 1
        while (k != 1):
            assert curr_cycle > 0, "curr_cycle cannot be less than 1"
            # odd
            if (k % 2 != 0):
                k = (k*3) + 1
                curr_cycle += 1
            # even
            else:
                k = (k // 2)
                curr_cycle += 1

        # update max_cycle if applicable
        if curr_cycle > max_cycle:
            max_cycle = curr_cycle

        # update curr_interval_max if applicable
        if add_to_cache:
            if curr_cycle > curr_interval_max:
                curr_interval_max = curr_cycle

    # add to cache if requested
    if add_to_cache:
        c[(num1, num2)] = curr_interval_max

    assert type(max_cycle) == int, "max value must be a integer"
    return max_cycle


def collatz_eval(i, j, c):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    # pre_condition
    assert i > 0, 'data needs to be a non-zero positive integer'
    assert j > 0, 'data needs to be a non-zero positive integer'
    assert i < 1000001, 'invalid input, integer needs to be <= 1000000'
    assert j < 1000001, 'invalid input, integer needs to be <= 1000000'

    # re-organize range if needed (first_num > second_num)
    if i > j:
        first_num = j
        second_num = i
    else:
        first_num = i
        second_num = j

    remainder_low = False
    remainder_high = False
    # asking if we should use cache
    if (second_num - first_num) >= 1000:
        # get range of numbers
        mod_num1 = first_num % 1000
        if mod_num1 != 0:
            factor1 = (first_num // 1000) + 1
            range_low = factor1 * 1000
            remainder_low = True
        else:
            range_low = first_num

        mod_num2 = second_num % 1000
        if mod_num2 != 0:
            factor2 = (second_num // 1000) - 1
            range_high = factor2 * 1000
            remainder_high = True
        else:
            range_high = second_num

        # check all ranges, if they DNE, create
        curr_num = range_low
        max_cycle = 0
        while curr_num <= range_high - 1000:
            r = (curr_num, curr_num + 1000)
            if r in c:
                if c[r] > max_cycle:
                    max_cycle = c[r]
                curr_num += 1000

            # get max cycle and store in cache
            else:
                max_cycle = get_cycle(
                    curr_num, curr_num+1000, max_cycle, add_to_cache=True, c=c)
                curr_num = curr_num+1000

        # check remainders if applicable
        if remainder_low:
            max_cycle = get_cycle(first_num, range_low, max_cycle)
        if remainder_high:
            max_cycle = get_cycle(range_high, second_num, max_cycle)

        # return max cycle for range > 1000 using cache
        assert max_cycle > 0, "max cycle cannot be less than 0"
        return max_cycle

    # return max cycle for < 1000 range numbers
    max_cycle = get_cycle(first_num, second_num)

    assert max_cycle > 0, "max cycle cannot be less than 0"
    assert type(max_cycle) == int, "max value must be a integer"
    return max_cycle

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    assert (type(i) == int and type(j) == int and type(v) == int), "collatz_print i,j,v must be ints"
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    c = {}
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j, c)
        collatz_print(w, i, j, v)
